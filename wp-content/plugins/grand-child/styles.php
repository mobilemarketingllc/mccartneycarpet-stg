<?php
// Variable for Product Loop listing page Column
$col_css = postpercol + 1;

$color = (get_option('api_colorcode_sandbox') != "")?get_option('api_colorcode_sandbox'):"#00247a";
$seleinformation = json_decode(get_option('saleinformation'));
$seleinformation = (object)$seleinformation;
?>
<style>
.salebanner-slide{
	max-height:440px!important;
	margin: 0 auto;
    display: block;
}
.homepage-banner-bg{
	background-image: url(<?php echo $seleinformation->background_image;?>)!important;
	background-repeat: no-repeat;
}

.fl-row-content-wrap.custombg {
    //background-image: url(<?php echo $seleinformation->background_image;?>)!important;
	background-image: url(<?php echo $seleinformation->background_image_landing;?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}
.fl-row-content-wrap.homepage {
    background-image: url(<?php echo $seleinformation->background_image;?>)!important;
//	background-image: url(<?php echo $seleinformation->background_image_landing;?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}

img.salebanner.fl-slide-photo-img {
    max-width: 500px;
}

.banner-mobile{
	display: none;
	text-align:center;
}

@media(max-width:1080px){
.fl-page-header-primary .fl-logo-img{
max-width: 80%;
}
}

@media(max-width:992px){
.fl-page-header-primary .fl-logo-img{
max-width: 50%;
}
.banner-below-heading-n-text h2 span{
font-size: 25px;
line-height: 28px;
}
.banner-deskop{
display: none;
}
.banner-mobile{
	display: block;
}
}
</style>
