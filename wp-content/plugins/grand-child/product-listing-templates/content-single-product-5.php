<div class="product-detail-layout-5">
<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;
$sku = get_field('sku') ;
$image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 
	
	if(strpos($image , 's7.shawimg.com') !== false){
		if(strpos($image , 'http') === false){ 
		$image = "http://" . $image;
	}	
		$image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
	}else{
		if(strpos($image , 'http') === false){ 
		$image = "https://" . $image;
	}	
		$image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
	}	
?>

	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
		<div class="fl-post-content clearfix grey-back" itemprop="text">
			<div id="product-colors">
			<?php
				$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider.php';
				include( $dir );
			?>
			</div>
				<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-6 col-sm-12 product-swatch">   
					<?php 
						$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images.php';
						include( $dir );
					?>
						<div class="clearfix"></div>
				</div>
				<div class="col-md-5 col-sm-12 col-md-offset-1 product-box">
					<div class="row">
						<div class="col-md-6">
							<?php if(get_field('parent_collection')) { ?>
							<h4><?php the_field('parent_collection'); ?></h4>
							<?php } ?>

							<?php if(get_field('collection')) { ?>
							<h1 class="fl-post-title" itemprop="name"><?php the_field('collection'); ?></h1>
							<?php } ?>

							<?php if(get_field('color')) { ?>
							<h2 class="fl-post-title" itemprop="name"><?php the_field('color'); ?></h2>
							<?php } ?>

							<?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5><?php*/ ?>
							
							<div class="product-colors">
								<?php
									$familysku = get_post_meta($post->ID, 'collection', true);                  
									$args = array(
										'post_type'      => $flooringtype,
										'posts_per_page' => -1,
										'post_status'    => 'publish',
										'meta_query'     => array(
											array(
												'key'     => 'collection',
												'value'   => $familysku,
												'compare' => '='
											),
											array(
												'key' => 'swatch_image_link',
												'value' => '',
												'compare' => '!='
												)
										)
									);
									$the_query = new WP_Query( $args );
								?>
								<ul>
									<li class="found"><?php echo $the_query ->found_posts; ?></li>
									<li class="colors">Colors<br/>Available</li>
								</ul>
							</div>
						</div>							
						<div class="product-colors col-md-6 text-right">
						<?php 					
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
							include_once( $dir );
						?>
						</div>
							<div class="clearfix"></div>
					</div>
					
					<div class="button-wrapper">
						
					<?php if( get_option('getcouponbtn') == 1){  ?>
											<a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="button alt getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
											<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?>
											</a>
            		<?php } ?>
						<a href="/contact-us/" class="button contact-btn">CONTACT US</a>
					<?php if(get_option('pdp_get_finance') != 1 || get_option('pdp_get_finance') == '' ){?>						
						<a href="/flooring-financing/" class="button text-right finance-btn">GET FINANCING</a>	
					<?php } ?>				
					</div>
						<div class="clearfix"></div>						
					<div id="product-attributes-wrap">
						<?php 
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
							include( $dir );
						?>
					</div>
						<div class="clearfix"></div>
				</div>
					<div class="clearfix"></div>
			</div>
				<div class="clearfix"></div>
			
			
		</div>
	</article>
</div>
<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>	