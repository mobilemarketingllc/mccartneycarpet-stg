<?php

/*
Template Name: Coolorwall Style Page 

*/

//Coolorwall Style Page Landings

 get_header();

    $post_types = 'luxury_vinyl_tile';

    $args = get_posts(array(
        'numberposts'	=> -1,
        'post_type'		=> $post_types,
        'meta_query'	=> array(
            array(
                'key'	 	=> 'color',
                'value'	  	=> $_GET['color'],
                'compare' 	=> '=',
            ),
            array(
                'key'	 	=> 'collection',
                'value'	  	=> 'COREtec Colorwall',
                'compare' 	=> '=',
            ),
        ),
    ));
    $the_query = new WP_Query( $args );
?>
<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
            <div class="style-list">
                <div class="top-heading">
                    <h5>COREtec Colorwall</h5>
                    <h3>
                        <span class="first-heading-text">2.</span>
                        <span class="second-heading-text">Choose your Style</span>
                    </h3>
                    <button type="button" class="back-button" onclick="history.back();">Back</button>
                </div>
                <div class="style-list-inner">
                    <ul>
                    <?php
                        $count = 0;
                        foreach( $the_query->query as $color ) {
                    ?>
                        <li <?php if($count==0){ ?>class="active"<?php } ?>>
                            <div class="style-li-inner">
                                <a href="javascript:void(0)" title="<?php echo get_field('style', $color->ID); ?>" data-id="tabcontent-<?php echo $color->ID ;?>">
                                    <div class="fl-post-grid-image prod_img">
                                        <?php
                                            $itemImage = get_field('swatch_image_link', $color->ID);
                                            $arrParsedUrl = parse_url($itemImage);
                                            if (empty($arrParsedUrl['scheme'])){
                                                $itemImage = 'https://'.$itemImage;
                                            } else{
                                                $itemImage = $itemImage;
                                            }
                                            
                                        ?>
                                        <img class="<?php echo $class; ?>" src="<?php echo $itemImage; ?>" alt="<?php echo get_field('style', $color->ID); ?>" />
                                    </div>
                                    <div class="product-info-box">
                                        <h6><?php echo get_field('style', $color->ID); ?></h6>
                                    </div>
                                </a>
                            </div>
                        </li>
                    <?php
                        $count++;
                        }
                    ?>
                    </ul>
                </div>
            </div>
            <div class="style-product_list">
                <?php foreach( $the_query->query as $color ) { ?>
                    <?php $swatchitemImage = get_field('swatch_image_link', $color->ID);
                            $roomsImage = get_field('gallery_room_images', $color->ID);
                            $arrParsedUrl = parse_url($roomsImage);
                            if (empty($arrParsedUrl['scheme'])){
                                $roomsImage = 'https://'.$roomsImage;
                            } else{
                                $roomsImage = $roomsImage;
                            }
                            $roomimg_url = 'https://mobilem.liquifire.com/mobilem?source=url['.$roomsImage.']&scale=size[910x812],options[crop]&sink';
                    ?>
                    <div class="pro-container" id="tabcontent-<?php echo $color->ID ;?>">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="pro-roomscene">
                                    <div class="main-image" style="background-image:url('<?php echo $roomimg_url;?>');">
                                        <img src="<?php echo $roomimg_url;?>" alt="<?php echo get_field('style', $color->ID); ?>" />
                                        <?php
                                            $collection = get_field('collection', $color->ID);
                                            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { 
                                        ?>
                                        <span class="exlusive-badge"><img src="<?php echo plugin_dir_url('__FILE__'); ?>grand-child/img/exclusive-products-icon.png" alt="<?php the_title(); ?>" /></span>
                                        <?php } ?>
                                    </div>

                                    <?php
                                        $image = get_field('swatch_image_link', $color->ID) ? get_field('swatch_image_link', $color->ID):"http://placehold.it/168x123?text=No+Image"; 

                                        if(strpos($image , 's7.shawimg.com') !== false){
                                            if(strpos($image , 'http') === false){ 
                                            $image = "http://" . $image;
                                        }	
                                            $image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[1600x1200]&sink";
                                        }else{
                                            if(strpos($image , 'http') === false){ 
                                            $image = "https://" . $image;
                                        }	
                                            $image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[1600x1200]&sink";
                                        }
                                    ?>
                                        <div class="image-thumbnails">
                                            <?php if (!empty($image)){ ?>
                                                <div class="toggle-image-holder"><a href="javascript:void(0)" class="product_gallery_img" data-background="<?php echo $image; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a></div>
                                            <?php } ?>
                                            <?php
                                            // check if the repeater field has rows of data
                                            if(get_field('gallery_room_images', $color->ID)){
                                                $gallery_images = get_field('gallery_room_images', $color->ID);
                                                $gallery_img = explode("|",$gallery_images);
                                                $coun = 0;
                                                // loop through the rows of data
                                                foreach($gallery_img as  $key=>$value) {
                                                    $room_image = $value;
                                                
                                                    if(strpos($room_image , 's7.shawimg.com') !== false){
                                                        if(strpos($room_image , 'http') === false){ 
                                                            $room_image = "http://" . $room_image;
                                                        }
                                                        $room_image_small = $room_image ;
                                                        $room_image = $room_image ;
                                                    } else{
                                                        if(strpos($room_image , 'http') === false){ 
                                                            $room_image = "https://" . $room_image;
                                                        }
                                                        $room_image_small= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[150]&sink";
                                                        $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[1600x1200]&sink";
                                                    }
                                                    if($coun==0){
                                                        $classname = "active";
                                                        $test = $room_image;
                                                    }
                                            ?>
                                                <div class="toggle-image-holder <?php echo $classname; ?>">
                                                    <a class="product_gallery_img" href="javascript:void(0)" data-background="<?php echo $room_image; ?>" style="background-image:url('<?php echo $room_image_small; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
                                                </div>
                                            <?php
                                                $coun++;
                                                }
                                            }
                                            ?>
                                            <div class="popup-icon">
                                                <a href="<?php echo $test; ?>" data-exthumbimage="<?php echo $test; ?>"><img src="<?php echo plugin_dir_url('__FILE__'); ?>grand-child/img/zoom-icon.png" /></a>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-5">    
                                <div class="pro-details">
                                    <div class="brand-img text-center"><img src="<?php echo plugin_dir_url('__FILE__'); ?>grand-child/img/COREtecLogoVRT_no-keyline.jpg" class="coretec_logo" alt="<?php echo get_field('collection',$color->ID); ?>" /></div>

                                    <h5 class="style_name"><?php echo get_field('style', $color->ID); ?></h5>
                                    <?php if(get_field('color',$color->ID)) { ?>
                                        <h6 class="color_name">Color: <strong><?php echo get_field('color', $color->ID); ?></strong></h6>
                                    <?php } ?>

                                    <div class="pro_description text-center"><?php echo get_field('description', $color->ID); ?></div>

                                    <div class="button-wrapper text-center">
                                        <a href="/flooring-coupon/" class="button alt">GET COUPON</a>
                                        <a href="/contact-us/" class="button">Contact Us</a>
                                    </div>

                                    <div class="product-attributes">
                                        <table class="table">
                                            <tbody>
                                                <?php if(get_field('parent_collection',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Main Collection</th>
                                                    <td><?php echo get_field('parent_collection',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                
                                                <?php if(get_field('collection',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Collection</th>
                                                    <td><?php echo get_field('collection',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php if(get_field('sku',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">SKU</th>
                                                    <td><?php echo get_field('sku',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php if(get_field('plank_dimensions',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Plank Dimensions</th>
                                                    <td><?php echo get_field('plank_dimensions',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php if(get_field('flooring_type',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Flooring Type</th>
                                                    <td><?php echo get_field('flooring_type',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php if(get_field('edge_profile',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Edge Profile</th>
                                                    <td><?php echo get_field('edge_profile',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php /*if(get_field('color',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Color</th>
                                                    <td><?php echo get_field('color',$color->ID); ?></td>
                                                </tr>
                                                <?php }*/ ?>
                                
                                                <?php if(get_field('construction',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Construction</th>
                                                    <td><?php echo get_field('construction',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                                
                                                <?php if(get_field('color_variation',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Color Variation</th>
                                                    <td><?php echo get_field('color_variation',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                            
                                                <?php if(get_field('shade',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Shade</th>
                                                    <td><?php echo get_field('shade',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php if(get_field('core',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Core</th>
                                                    <td><?php echo get_field('core',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                                
                                                <?php if(get_field('shape',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Shape</th>
                                                    <td><?php echo get_field('shape',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                            
                                                <?php if(get_field('species',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Species</th>
                                                    <td><?php echo get_field('species',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                                
                                                <?php if(get_field('fiber_type',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Fiber Type</th>
                                                    <td><?php echo get_field('fiber_type',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                            
                                                <?php if(get_field('surface_type',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Surface Type</th>
                                                    <td><?php echo get_field('surface_type',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                
                                                <?php if(get_field('surface_texture_facet',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Surface Texture</th>
                                                    <td><?php echo get_field('surface_texture_facet',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                                <?php if(get_field('style')) { ?>
                                                <tr>
                                                    <th scope="row">Style</th>
                                                    <td><?php echo get_field('style',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                            
                                                <?php if(get_field('edge',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Edge</th>
                                                    <td><?php echo get_field('edge',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                            
                                                <?php if(get_field('application',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Application</th>
                                                    <td><?php echo get_field('application',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                
                                                <?php if(get_field('size',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Size</th>
                                                    <td><?php echo get_field('size',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                            
                                                <?php if(get_field('width',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Width</th>
                                                    <td><?php echo get_field('width',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                
                                                <?php if(get_field('length',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Length</th>
                                                    <td><?php echo get_field('length',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                            
                                                <?php if(get_field('thickness',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Thickness</th>
                                                    <td><?php echo get_field('thickness',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                
                                                <?php if(get_field('installation')) { ?>
                                                <tr>
                                                    <th scope="row">Installation Method</th>
                                                    <td><?php echo get_field('installation',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                                
                                                <?php if(get_field('location',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Location</th>
                                                    <td><?php echo get_field('location',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                                
                                                <?php if(get_field('backing_facet',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Backing</th>
                                                    <td><?php echo get_field('backing_facet',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>
                                                
                                                <?php if(get_field('look',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Look</th>
                                                    <td><?php echo get_field('look',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php if(get_field('attached_underlayment',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Attached Underlayment</th>
                                                    <td><?php echo get_field('attached_underlayment',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php if(get_field('installation_method',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Installation Method</th>
                                                    <td><?php echo get_field('installation_method',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                                <?php if(get_field('installation_level',$color->ID)) { ?>
                                                <tr>
                                                    <th scope="row">Installation Level</th>
                                                    <td><?php echo get_field('installation_level',$color->ID); ?></td>
                                                </tr>
                                                <?php } ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                                <div class="clearfix"></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
            <?php wp_reset_query(); ?>

            <div class="choose-trim full-width-strech">
                <div class="full-width-inner">
                    <div class="top-heading">
                        <h3>
                            <span class="first-heading-text">3.</span>
                            <span class="second-heading-text">Choose your Trim</span>
                        </h3>
                    </div>
                    <div class="text-para">
                        <p>Your trim needs are determined by where you are installing your floor (i.e. stairs.)<br />
                        Your local Shaw Flooring Network Retailer can assist in selecting the appropriate coordinated trim for your project.</p>
    
                        <p><a href="/contact-us/" class="more-link">Contact Us</a></p>
                    </div>
                        <div class="clearfix"></div>
                </div>
            </div>
                <div class="clearfix"></div>
            <div class="warranty-section full-width-strech">
                <div class="full-width-inner">
                    <div class="top-heading">
                        <h5>Warranty</h5>
                    </div>
                    <div class="text-para">
                        <p>The COREtec Colorwall collection is backed by our comprehensive COREtec Plus Warranty Program, which includes a Limited Lifetime Residential Wear, Structure, Waterproof and Petproof Warranty, plus a 10-year Limited Medium Commercial Warranty.</p>

                        <p><a href="https://flooringyouwell.com/wp-content/uploads/2019/08/COREtec-Plus-Warranties.pdf" target="_blank" rel="noopener" class="down-pdf-text">Download our warranty</a></p>
                    </div>

                    <div class="divider-separator"></div>

                    <div class="certified-logos">
                        <div class="certified-logos-left">
                            <div class="text-right">
                                <img src="https://mmllc-images.s3.us-east-2.amazonaws.com/coretec-colorwall/landing-page/usa-flag.jpg" alt="USA Flag" />
                            </div>
                        </div>
                        <div class="certified-logos-center">
                            <div class="text-center">
                                <img src="https://mmllc-images.s3.us-east-2.amazonaws.com/coretec-colorwall/landing-page/greenguard-logo-450x600-copy.jpg" alt="Greenguard Certified" />
                            </div>
                        </div>
                        <div class="certified-logos-right">
                            COREtec luxury vinyl planks and tiles are manufactured in the U.S.A Every product is <strong>Greenguard Gold Certified</strong> and <strong>CARB 2 Compliant.</strong>
                        </div>
                    </div>
                </div>
            </div>
                <div class="clearfix"></div>
        </div>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
    
    jQuery('.image-thumbnails > .toggle-image-holder:not(.active)').each(function(){
        var getdatabg = jQuery(this).find('.product_gallery_img').data('background');
        jQuery(this).parents('.image-thumbnails:eq(0)').find('.popup-icon').append('<a href="'+getdatabg+'" data-exthumbimage="'+getdatabg+'" class="hidden" />');
    });

    jQuery('.popup-icon').lightGallery({
        thumbnail:true,
        exThumbImage: 'data-exthumbimage'
    }); 

    jQuery('.style-list ul > li > .style-li-inner > a').click(function(e){
        var getactid = jQuery(this).data('id');
        if(jQuery(this).parents('li:eq(0)').hasClass('active')){

        }
        else{
            jQuery('.style-list ul > li').removeClass('active');
            jQuery('.style-product_list .pro-container').hide();
            jQuery('#'+getactid).fadeIn();
            jQuery(this).parents('li:eq(0)').addClass('active');

            jQuery('html, body').animate({
                scrollTop: jQuery('#'+getactid).offset().top
            }, 1000);
        }
        e.preventDefault();
    });
    jQuery('.style-product_list .pro-container:eq(0)').fadeIn();

    //.main-image
    jQuery('.pro-roomscene .image-thumbnails .toggle-image-holder .product_gallery_img').click(function(e){
        e.preventDefault();
        var getCurrentImgURl = jQuery(this).data('background');
        jQuery('.pro-roomscene .image-thumbnails .toggle-image-holder').removeClass('active');
        jQuery(this).parents('.toggle-image-holder:eq(0)').addClass('active');
        jQuery('.pro-roomscene .main-image > img').attr('src', getCurrentImgURl);
        jQuery('.pro-roomscene .main-image').css('backgroundImage', 'url('+getCurrentImgURl+')');

        //jQuery('.pro-roomscene .image-thumbnails .popup-icon > a:eq(0)').attr('href', getCurrentImgURl).attr('data-exthumbimage', getCurrentImgURl);
    });

});
</script>
<?php get_footer(); ?>

