<?php
/*
Plugin Name: MM AccessiBe
Plugin URI: http://www.wp-code.com/
Description: MM accessiBe plugin is an Automatic AI Solution for Web Accessibility & Compliance with WCAG 2.1 AA Level and US Section 508, Americans with Disabilities Act (ADA) and European Union Standard EN 301549
Author: MM
Version: 1.2
Author URI: https://wpmaster.mm-dev.agency
*/


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    //'https://bitbucket.org/user-name/repo-name',
    'https://bitbucket.org/mobilemarketingllc/mm-accessibe',
	__FILE__,
	'mm-accessibe'
);

$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
	'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));

//Optional: Set the branch that contains the stable release.
    $myUpdateChecker->setBranch('master');
    



//accessiBe script hook with cde parameter
add_action('wp_footer', 'mmaccessiBe_custom_footer_js',200);

function mmaccessiBe_custom_footer_js() {
    
  echo "<script > (function(document, tag) {
    var script = document.createElement(tag);
    var element = document.getElementsByTagName('body')[0];
    script.src = 'https://acsbap.com/api/app/assets/js/acsb.js';
    script.async = true;
    script.defer = true;
    (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script);
    script.onload = function() {
        acsbJS.init({
            statementLink: '',
            feedbackLink: '',
            footerHtml: 'Enable powered by <a href=https://accessibe.com/a/3pkypfv target=_blank>accessiBe</a>',
            hideMobile: false,
            hideTrigger: false,
            language: 'en',
            position: 'left',
            leadColor: '#c8c8c8',
            triggerColor: '#146ff8',
            triggerRadius: '50%',
            triggerPositionX: 'left',
            triggerPositionY: 'bottom',
            triggerIcon: 'wheel_chair4',
            triggerSize: 'medium',
            triggerOffsetX: 20,
            triggerOffsetY: 20,
            mobile: {
                triggerSize: 'small',
                triggerPositionX: 'left',
                triggerPositionY: 'center',
                triggerOffsetX: 0,
                triggerOffsetY: 0,
                triggerRadius: '0'
            }
        });
    };
}(document, 'script')); </script>";
}



