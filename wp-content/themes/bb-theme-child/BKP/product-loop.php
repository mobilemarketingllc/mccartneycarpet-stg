<div class="product-grid swatch" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row">
<?php while ( have_posts() ): the_post(); ?>
    <div class="col-md-3 col-sm-4 col-xs-6">
    <!-- <div class="fl-post-grid-post" itemscope itemtype="<?php //FLPostGridModule::schema_itemtype(); ?>"> -->
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')!='' && get_field('swatch_image_link')!='http://') { ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <div class="fl-post-grid-image" style="background: url('https://mobilem.liquifire.com/mobilem?source=url[<?php the_field('swatch_image_link'); ?>]&scale=size[170x170]&sink') center center / cover no-repeat;">

                    <?php //the_post_thumbnail($settings->image_size); ?>
                   <!--  <img src="<?php //the_field('swatch_image_link'); ?>" alt="<?php //the_title_attribute(); ?>" /> -->

            </div>
        </a>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h4><?php the_field('collection'); ?></h4>
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>
            <a href="<?php echo site_url(); ?>/flooring-promotions/coupon/" target="_self" class="fl-button displaybtn" role="button">
                <span class="fl-button-text">GET COUPON</span>
            </a> 
            <a class="link" href="<?php the_permalink(); ?>">VIEW PRODUCT</a>
        </div>
    </div>
    </div>
<?php endwhile; ?>
</div>
</div>