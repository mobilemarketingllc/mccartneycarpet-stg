<?php
//$show_thumbs = FLTheme::get_setting('fl-posts-show-thumbs');
?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">

    <header class="fl-post-header">

        <?php //FLTheme::post_top_meta(); ?>
    </header><!-- .fl-post-header -->

    <div class="fl-post-content clearfix grey-back" itemprop="text">

        <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 product-swatch">

             <?php if(get_field('swatch_image_link')!='' && get_field('swatch_image_link')!='http://') { ?>

                <div class="img-responsive toggle-image" style="background-image:url('https://mobilem.liquifire.com/mobilem?source=url[<?php the_field('swatch_image_link'); ?>]&scale=size[470x470]&sink');background-size: cover;">
                    <img src="https://mobilem.liquifire.com/mobilem?source=url[<?php the_field('swatch_image_link'); ?>]&scale=size[600x600]&sink" class="img-responsive toggle-image" />
                </div>
             <?php }else{ ?>

                <div class="img-responsive toggle-image" style="background-image:url('http://placehold.it/600x600?text=No+Image');background-size: cover;">
                    <img src="http://placehold.it/600x600?text=No+Image" class="img-responsive toggle-image" />
                </div>

             <?php    }  ?>

                <?php if(get_field('room_scene_image_link')!='' && get_field('room_scene_image_link')!='http://' ){ ?>
                    <div class="toggle-image-thumbnails">
                        <?php 
                        $a=array();

                        if(get_field('swatch_image_link')=='http://' &&  get_field('room_scene_image_link')!='http://')
                   {
                        $a[]='http://placehold.it/470x470?text=No+Image';
                       // $a[]=get_field('swatch_image_link');
                        $a[]=get_field('room_scene_image_link');

                   } else if (get_field('swatch_image_link')!='http://' &&  get_field('room_scene_image_link')=='http://')
                    {
                   
                   $a[]=get_field('swatch_image_link');
                   //$a[]=get_field('room_scene_image_link');
                   $a[]='http://placehold.it/470x470?text=No+Image';
                     }
                     else if (get_field('swatch_image_link')=='http://' &&  get_field('room_scene_image_link')=='http://')
                        {
                       
                            $a[]='http://placehold.it/470x470?text=No+Image';
                            $a[]='http://placehold.it/470x470?text=No+Image';
                         }
                   
                   else {

                    $a[]=get_field('swatch_image_link');
                   $a[]=get_field('room_scene_image_link');

                   }
                        foreach($a as $k=>$v){
                            ?><a href="#" data-background="https://mobilem.liquifire.com/mobilem?source=url[<?php echo $v ?>]&scale=size[470x470]&sink" data-fr-replace-bg=".toggle-image" style="background-image:url('https://mobilem.liquifire.com/mobilem?source=url[<?php echo $v ?>]&scale=size[50x50]&sink');background-size: cover;"></a><?php
                        }
                        ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-6 col-sm-6 product-box">

                <?php get_template_part('includes/product-brand-logos'); ?>

                <?php if(get_field('parent_collection')) { ?>
                <h2><?php the_field('parent_collection'); ?> </h2>
                <?php } ?>


                <h2><?php the_field('collection'); ?></h2>
                <h1 class="fl-post-title" itemprop="name">
                   <?php the_field('color'); ?>
                </h1>

                <div class="product-colors">
                    <?php
                    $familysku = get_post_meta($post->ID, 'collection', true);
                    if(is_singular( 'laminate' )){
                        $flooringtype = 'laminate';
                    } elseif(is_singular( 'hardwood' )){
                        $flooringtype = 'hardwood';
                    } elseif(is_singular( 'carpeting' )){
                        $flooringtype = 'carpeting';
                    } elseif(is_singular( 'luxury_vinyl_tile' )){
                        $flooringtype = 'luxury_vinyl_tile';
                    } elseif(is_singular( 'vinyl' )){
                        $flooringtype = 'vinyl';
                    } elseif(is_singular( 'solid_wpc_waterproof' )){
                        $flooringtype = 'solid_wpc_waterproof';
                    } elseif(is_singular( 'tile' )){
                        $flooringtype = 'tile';
                    }

                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => 'collection',
                                'value'   => $familysku,
                                'compare' => '='
                            )
                        )
                    );
                    ?>
                    <?php
                    $the_query = new WP_Query( $args );
                    ?>
                    <ul>
                        <li class="found"><?php  echo $the_query ->found_posts; ?></li>


                        <li class="colors">Colors<br/>Available</li>
                    </ul>

                </div>

                <a href="<?php echo site_url(); ?>/coupon/" class="fl-button displaybtn" role="button" style="width: auto;">
                    <span class="fl-button-text">GET COUPON</span>
                </a>
<br />
                <a href="<?php echo get_page_link(13); ?>" class="fl-button btn-white" role="button" style="width: auto;">
                    <span class="fl-button-text">CONTACT US FOR INFO</span>
                </a>
                <br />
                <a href="<?php echo get_page_link(284); ?>">FREE IN-HOME ESTIMATE ></a>





                <div class="product-atts">

                </div>
            </div>
        </div>


        <?php get_template_part('includes/product-color-slider'); ?>
</div>



    </div><!-- .fl-post-content -->
<div class="container">
    <?php get_template_part('includes/product-attributes'); ?>
</div>

    <?php //FLTheme::post_bottom_meta(); ?>
    <?php //FLTheme::post_navigation(); ?>
    <?php //comments_template(); ?>

</article>
<!-- .fl-post -->