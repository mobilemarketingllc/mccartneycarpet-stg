<div class="product-grid rooms" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row">
<?php while ( have_posts() ): the_post(); ?>
    <div class="col-md-4 col-sm-6 col-xs-6">
    <!-- <div class="fl-post-grid-post" itemscope itemtype="<?php //FLPostGridModule::schema_itemtype(); ?>"> -->
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('room_scene_image_link')) { ?>
            <div class="fl-post-grid-image-room">

                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <div class="grid-room" style=" position: relative; width: 100%; height: 300px; background: url(<?php the_field('room_scene_image_link'); ?>) no-repeat bottom center; background-size: cover;">
                        <div class="in-stock" style="font-size: 90%; position: absolute; top: 0; left: 0; padding: 5px 15px;; background-color: #000; color:  #fff;">In-Stock</div>
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <!-- <img src="<?php //the_field('room_scene_image_link'); ?>" alt="<?php //the_title_attribute(); ?>" /> -->
                    </div>
                </a>

            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image-room">

                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <div class="grid-room" style=" position: relative; width: 100%; height: 300px; background: url(<?php the_field('swatch_image_link'); ?>) no-repeat bottom center; background-size: cover;">
                        <div class="in-stock" style="font-size: 90%; position: absolute; top: 0; left: 0; padding: 5px 15px;; background-color: #000; color:  #fff;">In-Stock</div>
                        <?php //the_post_thumbnail($settings->image_size); ?>
                        <!-- <img src="<?php //the_field('room_scene_image_link'); ?>" alt="<?php //the_title_attribute(); ?>" /> -->
                    </div>
                </a>



            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">
            <h2 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('color'); ?></a>
            </h2>
            <h4><?php the_field('collection'); ?></h4>
            <?php if(get_field('stikethrough_price')) { ?>
<span class="strike-price">$<?php the_field('stikethrough_price'); ?></span>
            <?php } ?>

            <?php if(get_field('sale_price')) { ?>
                <span class="sale-price">$<?php the_field('sale_price'); ?> sq. ft.</span>
            <?php } ?>



        </div>
    </div>
    </div>
<?php endwhile; ?>
</div>
</div>