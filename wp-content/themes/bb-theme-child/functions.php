<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
  wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
  wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
  wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
  wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'gallery-menu' => __( 'Gallery Menu' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  $params['total'] . ' Products';
    return $output;
}, 10, 2 );

// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');
function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

function new_year_number_2() 
{
	return $new_year = date('y');
}
add_shortcode('year_code_2', 'new_year_number_2');

add_action('fl_after_content', 'my_global_footer');
  function my_global_footer(){
   echo do_shortcode('[fl_builder_insert_layout slug="footer-1"]');
 };

 
//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
  if ( isset( $atts['facet'] ) ) {       
      $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
  }
  return $output;
}, 10, 2 );