var $=jQuery;

$(window).ready(function(){


    fr_add_filter("fr_slider_init",function(settings,slider){
        if($(window).width()<726){
            if(slider.is(".color_variations_slider")){
                settings.slidesToScroll=3;
                settings.slidesToShow=3;
            }
        }
        return settings;
    });

    //SLIDER
    //Example: {"slidesToScroll":7,"slidesToShow":7,"arrows":false,"infinite": false}
    //More options can be found here: http://kenwheeler.github.io/slick/
    if ($().slick) {
        fr_slider_init(".fr-slider");

        $("body").on("click",".fr-slider .prev",function(e){
          e.preventDefault();
          $(this).parents(".fr-slider").first().find(".slides").slick('slickPrev');
        });
        $("body").on("click",".fr-slider .next",function(e){
          e.preventDefault();
          $(this).parents(".fr-slider").first().find(".slides").slick('slickNext');
        });
    }

    function fr_slider_init(ob){
      $(ob).each(function(){
        var default_settings={slidesToScroll:1,slidesToShow:1}
        var settings=fr_parse_attr_data($(this).attr("data-fr"));
        settings=$.extend(default_settings,settings);
        settings=fr_apply_filter("fr_slider_init",settings,[$(ob)]);
        $(ob).find(".slides").slick(settings);


        $(ob).find(".slides").on('beforeChange', function(event, slick, currentSlide, nextSlide){
          next_slide(this,nextSlide);
        });

        function next_slide(ob,nextSlide){
          if(nextSlide==0){
            $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({opacity:0});
          }else{
            $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({opacity:1});
          }

          if(nextSlide>=$(ob).find(".slide").length-settings.slidesToScroll){
            $(ob).parents(".fr-slider").first().find(".arrow.next").animate({opacity:0});
          }else{
            $(ob).parents(".fr-slider").first().find(".arrow.next").animate({opacity:1});
          }
        }

        next_slide($(ob).find(".slides"),0);

        $(ob).addClass("init");
      });
    }

    function fr_slider_delete(ob){
      $(ob).each(function(){
        $(this).find(".slides").slick('unslick');
      });
    }






    //REPLACERS
    $("body").on("click","[data-fr-replace-src]",function(e){
        e.preventDefault();
        $($(this).attr("data-fr-replace-src")).attr("src",$(this).attr("data-src"));
    });

    $("body").on("click","[data-fr-replace-bg]",function(e){
        e.preventDefault();
        $($(this).attr("data-fr-replace-bg")).css("background-image","url("+$(this).attr("data-background")+")");
    });





    $("body").on("click",".open-gallery-modal",function(e){
        e.preventDefault();

        if(!$(".active-gallery-modal").length){
            $(".fl-page-content").prepend("<div class='active-gallery-modal'></div>");
        }

        var html=$(this).find(".gallery-modal").html();
        if($(".active-gallery-modal").is(":visible")){
            close_modal(function(){
                open_modal(html);
            });
        }else{
            open_modal(html);
        }
    });

    $("body").on("click",".active-gallery-modal .close_modal",function(e){
        e.preventDefault();
        close_modal();
    });

    //$(".open-gallery-modal").first().click();

    function close_modal(func){
        $(".active-gallery-modal").fadeOut(func);
    }

    function open_modal(html){
        var header=$(".fl-page-header").outerHeight();
        if(!$(".fl-page-header").is(":visible")){
            header=$("#djcustom-header").outerHeight();
        }console.log(header);
        $(".active-gallery-modal").css("top",header+$(window).scrollTop());
        $(".active-gallery-modal").html(html);
        $(".active-gallery-modal").fadeIn();
    }


    var hash = window.location.hash.substr(1);

    $(".fl-tabs-label").each(function(){
        if($.trim($(this).text()).toLowerCase()==hash.toLowerCase()){console.log($(this));
            change_tab($(this));
            return false;
        }
    });


    $("footer .menu-item-has-children").click(function(e){
        if($(window).width()<=768){
            e.preventDefault();
            $(this).find(">ul").toggle();
        }
    });

    $("header#djcustom-header .fl-photo").before("<a class='show_menu' href='#'><i class='fa fa-bars'></i></a>");
    $("header#djcustom-header .fl-photo").before("<a class='close_search' href='#'><i class='fa fa-close'></i></a>");

    $("header#djcustom-header").append("<div class='search_wrapper'>"+$(".fl-page-nav-search").html()+"</div>");
    $("header#djcustom-header .search_wrapper>a").click(function(e){
        e.preventDefault();
        $(this).next().submit();
    });

    $("header .fl-photo").before("<a href='#' class='search_mobile'><i class='fa fa-search'></i></a>");


    $("header#djcustom-header .search_mobile").click(function(e){
        e.preventDefault();
        $("header#djcustom-header .search_wrapper").slideToggle();
    });



    $(".show_menu").click(function(e){
        e.preventDefault();
        $(".sc_mega_menu").fadeIn();
        $("header#djcustom-header .sc_mega_menu .mega-menu>ul").each(function(){
            $(this).parent().addClass("has_subitems");
            $(".sc_mega_menu .fl-page-nav-collapse").prepend("<ul class='secondary_menu nav navbar-nav menu'></ul>");
            $("header#djcustom-header .show_menu").hide();
            $("header#djcustom-header .fl-photo").before("<a class='close_menu' href='#'><i class='fa fa-close'></i></a>");

            var menu = $(this).parents(".sc_mega_menu .fl-page-nav-wrap");

            $("header#djcustom-header .close_menu").click(function(e){
                e.preventDefault();
                $(".sc_mega_menu").fadeOut();
                $("header#djcustom-header .show_menu").show();
                $(this).hide();
            });

            $(this).parent().unbind().click(function(e){
                var ob=$(this);
                e.preventDefault();
                menu.animate({"margin-left":-menu.outerWidth()},function(){
                    if(!$(".sc_mega_menu").is(".active")){console.log(1);
                        $(".sc_mega_menu .secondary_menu").html("<li class='main_link'>"+$(ob).html()+"</li>");
                        $(".sc_mega_menu").addClass("active");

                        $(".main_link>a").click(function(e){
                            e.preventDefault();
                            menu.animate({"margin-left":-menu.outerWidth()},function(){
                                $(".sc_mega_menu .secondary_menu").html("");
                                $(".sc_mega_menu").removeClass("active");
                                menu.animate({"margin-left":0});
                            });
                        });
                    }else{
                        $(".sc_mega_menu").removeClass("active");
                    }
                    menu.animate({"margin-left":0});
                });
            });
        });
    });
});


function change_tab(ob){
    ob.parent().find(".fl-tab-active").removeClass("fl-tab-active");
    ob.addClass("fl-tab-active");
    var index=ob.index();

    ob.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-label").removeClass("fl-tab-active");
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-panel-content").removeClass("fl-tab-active");
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel:eq("+index+") .fl-tabs-panel-content").addClass("fl-tab-active");
}




function fr_parse_attr_data(data){
  if(!data){
    data="";
  }
  if(data.substr(0,1)!="{"){
    data="{"+data+"}";
  }
  return $.parseJSON(data);
}

//FILTER SYSTEM LIKE IN WORDPRESS
var fr_filters=[];
function fr_add_filter(filter,func){
  filter=filter;
    if(typeof fr_filters[filter]=="undefined"){
        fr_filters[filter]=[];
    }
    fr_filters[filter][fr_filters[filter].length]=func;
}

function fr_apply_filter(filter,res,args){
    if(typeof fr_filters[filter]!="undefined"){
        for(k in fr_filters[filter]){
            var args2=[res].concat(args);
            res=fr_filters[filter][k].apply(null,args2);
        }
    }
    return res;
}
